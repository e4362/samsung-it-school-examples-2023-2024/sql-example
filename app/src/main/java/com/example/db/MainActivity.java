package com.example.db;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    String SQLRequest = "SELECT `tracks`.`TrackId`, `tracks`.`Name`, `tracks`.`Composer`,\n" +
            "`tracks`.`Milliseconds`, `albums`.`Title` as `AlbumName`, \n" +
            "`genres`.`Name` as `Genre`\n" +
            "FROM `tracks`\n" +
            "LEFT JOIN `albums` ON `albums`.`AlbumId` = `tracks`.`AlbumId`\n" +
            "LEFT JOIN `genres` ON `genres`.`GenreId` = `tracks`.`GenreId`; ";
//        String SQLRequest = "SELECT \n" +
//        "    name\n" +
//        "FROM \n" +
//        "    sqlite_schema\n" +
//        "WHERE \n" +
//        "    type ='table' AND \n" +
//        "    name NOT LIKE 'sqlite_%';";

        ListView listView;

    String[] from = {"trackName", "composer", "length", "albumName", "genre"};
    int[] to = {R.id.trackNameText, R.id.composerNameText, R.id.lengthText, R.id.albumNameText,
    R.id.genreNameText};
    ArrayList<Map<String, Object>> data;
    Map<String, Object> m;
    SimpleAdapter simpleAdapter;
    SQLiteDatabase db;
    DataBaseHelper dataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_view);

        dataBaseHelper = new DataBaseHelper(this);
        dataBaseHelper.openDatabase();
        db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(SQLRequest, null);

        data = new ArrayList<>();

        while (cursor.moveToNext()){
            Log.d("DB", cursor.getString(0));
            m = new HashMap<String, Object>();
            m.put("trackName", cursor.getString(1));
            m.put("composer", cursor.getString(2));
            m.put("length", cursor.getString(3));
            m.put("albumName", cursor.getString(4));
            m.put("genre", cursor.getString(5));
            data.add(m);
        }
        simpleAdapter = new SimpleAdapter(this, data, R.layout.list_item, from, to);

        listView.setAdapter(simpleAdapter);
    }
}